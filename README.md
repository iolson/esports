# E-Sports

Laravel 5 Package for creating an e-sports team website. This will hanlde everything even a basic template to be used.

## Documentation

Coming Soon

## Changelog

Coming Soon

## Support

The following support channels can be used for contact.

- [Twitter](https://twitter.com/odotmedia)
- [Email](mailto:info@odotmedia.com)

Bug reports, feature requests, and pull requests can be submitted by following our [Contribution Guide](CONTRIBUTING.md).

## Contributing & Protocols

- [Versioning](CONTRIBUTING.md#versioning)
- [Coding Standards](CONTRIBUTING.md#coding-standards)
- [Pull Requests](CONTRIBUTING.md#pull-requests)


## License

This software is released under the [MIT](LICENSE) License.


&copy; 2015 Odot Media LLC. All rights reserved.