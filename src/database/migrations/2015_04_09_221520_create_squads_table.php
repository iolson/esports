<?php

use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateSquadsTable extends Migration {

	/**
	 * Run the migrations.
	 *
	 * @return void
	 */
	public function up()
	{
		Schema::create('squads', function(Blueprint $table)
		{
			$table->increments('id');
            $table->unsignedInteger('game_id');
            $table->string('title');
            $table->string('slug');
            $table->string('banner_url');
			$table->timestamps();
		});
	}

	/**
	 * Reverse the migrations.
	 *
	 * @return void
	 */
	public function down()
	{
		Schema::drop('squads');
	}

}
