<?php namespace Odotmedia\Esports;

use Illuminate\Support\ServiceProvider;

class EsportsServiceProvider extends ServiceProvider
{
    /**
     * Indicates if loading of the provider is deferred.
     *
     * @var bool
     */
    protected $defer = false;

    /**
     * {@inheritDoc}
     */
    public function boot()
    {
        //
    }

    /**
     * {@inheritDoc}
     */
    public function register()
    {
        $this->prepareResources();
    }

    protected function prepareResources()
    {
        // Publish config
        $config = realpath(__DIR__.'/../../config/config.php');

        $this->publishes([
            $config => config_path('odotmedia.esports.php'),
        ], 'config');

        // Publish migrations
        $migrations = realpath(__DIR__.'/../../database/migrations');

        $this->publishes([
            $migrations => $this->app->databasePath().'/migrations',
        ], 'migrations');
    }
}