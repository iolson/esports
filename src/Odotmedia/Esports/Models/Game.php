<?php namespace Odotmedia\Esports\Models;

use Illuminate\Database\Eloquent\Model;

/**
 * Game
 *
 * This model will hold the different games that your organization would like to record stuff and have squads for.
 *
 * @package Odotmedia\Esports
 * @author  Ian Olson <ian@odotmedia.com>
 */
class Game extends Model
{
    /**
     * Model table.
     *
     * @var string
     */
    protected $table = 'games';
}