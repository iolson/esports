<?php namespace Odotmedia\Esports\Models;

use Illuminate\Database\Eloquent\Model;

/**
 * Category
 *
 * This model will hold your categories for your Article model.
 *
 * @package Odotmedia\Esports
 * @author  Ian Olson <ian@odotmedia.com>
 */
class Category extends Model
{
    /**
     * Model table.
     *
     * @var string
     */
    protected $table = 'categories';
}
