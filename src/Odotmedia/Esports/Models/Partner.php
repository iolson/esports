<?php namespace Odotmedia\Esports\Models;

use Illuminate\Database\Eloquent\Model;

/**
 * Partner
 *
 * This model will hold your sponsors, partners and supporters for your organization.
 *
 * @package Odotmedia\Esports
 * @author  Ian Olson <ian@odotmedia.com>
 */
class Partner extends Model
{
    /**
     * Model table.
     *
     * @var string
     */
    protected $table = 'partners';
}