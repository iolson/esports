<?php namespace Odotmedia\Esports\Models;

use Illuminate\Database\Eloquent\Model;

class Map extends Model
{
    protected $table = 'maps';

    public function game()
    {
        return $this->belongsTo('App\Game');
    }
}
