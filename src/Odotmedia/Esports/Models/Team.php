<?php namespace Odotmedia\Esports\Models;

use Illuminate\Database\Eloquent\Model;

/**
 * Team
 *
 * This model is used to house your organization and your opponents that you play. This will be used mainly in the
 * matches area, so you won't have to re-type if you play the same team again.
 *
 * @package Odotmedia\Esports
 * @author  Ian Olson <ian@odotmedia.com>
 */
class Team extends Model
{
    /**
     * Model table.
     *
     * @var string
     */
    protected $table = 'teams';
}