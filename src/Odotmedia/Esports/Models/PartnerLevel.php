<?php namespace Odotmedia\Esports\Models;

use Illuminate\Database\Eloquent\Model;

/**
 * PartnerLevel
 *
 * This model will be for the levels of the partnerships.
 *
 * i.e. Head Sponsor, Sponsor, Supporter
 *
 * @package Odotmedia\Esports
 * @author  Ian Olson <ian@odotmedia.com>
 */
class PartnerLevel extends Model
{
    protected $table = 'partner_levels';
}