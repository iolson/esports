<?php namespace Odotmedia\Esports\Models;

use Illuminate\Database\Eloquent\Model;

/**
 * Competition
 *
 * This model will hold your competitions which include leagues and tournaments. This will be what your matches and
 * scores will be connected to.
 *
 * @package Odotmedia\Esports
 * @author  Ian Olson <ian@odotmedia.com>
 */
class Competition extends Model
{
    /**
     * Model table.
     *
     * @var string
     */
    protected $table = 'competitions';

    /**
     * Get Game of the Competition
     *
     * Examples:
     *
     * Competition::find(1)->game->title
     *
     * @return \Illuminate\Database\Eloquent\Relations\BelongsTo
     */
    public function game()
    {
        return $this->belongsTo('App\Game');
    }

    /**
     * Get Matches
     *
     * Examples:
     *
     * $matches = Competition::find(1)->matches
     *
     * foreach ($matches as $match) {
     *      var_dump($match);
     * }
     *
     * @return \Illuminate\Database\Eloquent\Relations\HasMany
     */
    public function matches()
    {
        return $this->hasMany('App\Match');
    }
}