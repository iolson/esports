<?php namespace Odotmedia\Esports\Models;

use Illuminate\Database\Eloquent\Model;

/**
 * Meta
 *
 * This model will hold all of the User Meta for Profiles for Users, so we can easily add and remove ones that need to
 * be updated, without disrupting the users table.
 *
 * @package Odotmedia\Esports
 * @author  Ian Olson <ian@odotmedia.com>
 */
class Meta extends Model
{
    protected $table = 'user_meta';

    /**
     * The attributes excluded from the model's JSON form.
     *
     * @var array
     */
    protected $hidden = ['id', 'user_id', 'updated_at', 'created_at'];
}
