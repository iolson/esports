<?php namespace Odotmedia\Esports\Models;

use Illuminate\Database\Eloquent\Model;

/**
 * Match
 *
 * This model will hold matches for a competition.
 *
 * @package Odotmedia\Esports
 * @author  Ian Olson <ian@odotmedia.com>
 */
class Match extends Model
{
    /**
     * Model table.
     *
     * @var string
     */
    protected $table = 'matches';

    /**
     * Get Competition that Match Belongs To
     *
     * This will get the competition that the match belongs to, this is mainly used for single view of a match to
     * properly show the competition details on the match view.
     *
     * @return mixed
     */
    public function competition()
    {
        return $this->belongsTo('Odotmedia\Esports\Models\Competition', 'competition_id');
    }

    /**
     * Get Scores of a Match
     *
     * This is in the case of a Best of 3 or more, as a match will have multiple games played, which will result in
     * multiple scores attributed to the match.
     *
     * @return mixed
     */
    public function scores()
    {
        return $this->hasMany('Odotmedia\Esports\Models\Score');
    }
}