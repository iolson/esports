<?php namespace Odotmedia\Esports\Models;

use Illuminate\Database\Eloquent\Model;

/**
 * Score
 *
 * This model holds the individual scores for a Match, as matches can be Best of 1 and up as high as a Best of 7.
 *
 * @package Odotmedia\Esports
 * @author  Ian Olson <ian@odotmedia.com>
 */
class Score extends Model
{
    /**
     * Model table.
     *
     * @var string
     */
    protected $table = 'scores';

    public function match()
    {
        return $this->belongsTo('App\Match', 'match_id');
    }
}