<?php namespace Odotmedia\Esports\Models;

use Illuminate\Database\Eloquent\Model;

/**
 * Article
 *
 * This model will hold your news articles.
 *
 * @package Odotmedia\Esports
 * @author  Ian Olson <ian@odotmedia.com>
 */
class Article extends Model
{
    /**
     * Model table.
     *
     * @var string
     */
    protected $table = 'articles';

    public function categories()
    {
        return $this->hasMany('App/Category');
    }
}