<?php namespace Odotmedia\Esports\Models;

use Illuminate\Database\Eloquent\Model;

/**
 * Squad
 *
 * This model is used to have your organizations squads attached to players.
 *
 * @package Odotmedia\Esports
 * @author  Ian Olson <ian@odotmedia.com>
 */
class Squad extends Model
{
    /**
     * Model table.
     *
     * @var string
     */
    protected $table = 'squads';

    public function players()
    {
        return $this->hasMany('App\Player');
    }
}